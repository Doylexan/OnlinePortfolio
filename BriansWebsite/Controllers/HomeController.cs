﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using BriansWebsite.Models;
using System.Net;

namespace BriansWebsite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var FormData = new FormData();

            return View(FormData);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Resume()
        {

            return View();
        }

        public ActionResult Portfolio()
        {

            return View();
        }

        public ActionResult SendEmail(FormData form)
        {
            MailMessage myMail = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("btrumpetdude926@gmail.com", "Theunitedfront55!");

            var fromAddress = new MailAddress(form.Email, form.FirstName + " " + form.LastName);
            //var toAddress = new MailAddress("briangarzat926@gmail.com", "Brian Garza");
            myMail.Subject = "Job Inquiry";
            myMail.Body = "Hello from " + form.Email + ",\n\n" + form.Message + "\n\n" + form.PhoneNum;

            using(var message = new MailMessage("btrumpetdude926@gmail.com", "briangarzat926@gmail.com"))
            {
                message.Subject = "Job Inquiry";
                message.Body = myMail.Body;
                smtp.Send(message);

            }

            return View("Index", new FormData());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}