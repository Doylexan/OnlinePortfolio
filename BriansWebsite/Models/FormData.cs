﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BriansWebsite.Models
{
    public class FormData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        [EmailAddress(ErrorMessage = "Email address is not valid")]
        public string Email { get; set; }
        public string PhoneNum { get; set; }
        public string PhoneNumTwo { get; set; }
        public string PhoneNumThree { get; set; }
        public string Message { get; set; }
    }
}